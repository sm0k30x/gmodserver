This container creates a Gmod server without any other games hooked up to it. [Show on Docker Hub](https://hub.docker.com/r/sm0k30x/docker-gmodserver/)

### Usage

    mkdir -p /mnt/docker/garrysmod
    chmod 777 /mnt/docker/garrysmod
    docker pull sm0k3/docker-gmodserver
    docker run -d --name gmodserver -v /mnt/docker/garrysmod:/opt/garrysmod -p 27015:27015/udp -p 27015:27015/tcp -e sm0k3/docker-gmodserver -game garrysmod +gamemode sandbox +map gm_flatgrass
You can change the gamemode and map or add your own parameters.
