FROM zennoe/steamcmd

LABEL   Name="GmodServer" \
        Maintainer="Sm0k3 <sm0k3@svalbard.cf>" \
        Version="0.1.0"

USER root

COPY data/bin /usr/local/bin/
COPY data/config.json /home/steam/fm-config.json

# install Usefull packages
RUN apt-get -y update && \
    apt-get -y install git wget curl zip unzip python python-pip pv tmux lib32tinfo5 openssh-server nodejs- && \
    apt-get -y clean

# fix permissions then install filebrowser
RUN chmod 777 /usr/local/bin/docker-entrypoint.sh && \
    mkdir /opt/garrysmod && \
    chown -R steam:steam /opt/garrysmod && \
    chmod 777 /usr/local/bin/megadown && \
    chmod 777 /usr/local/bin/helpers.py && \
    chmod 777 /usr/local/bin/gconsole && \
    chown steam:steam /home/steam/fm-config.json && \
    curl https://getcaddy.com | bash -s personal && \
    curl -fsSL https://filebrowser.github.io/get.sh | bash
USER steam
ENV LD_LIBRARY_PATH=/opt/garrysmod/bin
VOLUME /opt/garrysmod
EXPOSE 27015/udp 27015/tcp 27005/udp 27125/tcp 27159/tcp

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["-game", "garrysmod", "+gamemode", "sandbox", "+map", "gm_flatgrass"]
