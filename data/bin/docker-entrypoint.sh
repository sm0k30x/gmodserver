#!/bin/bash

gbasedir=/opt/garrysmod
fastdlfolder=${gbasedir}/fastdl/
fastdl_port=27159

ModFile=${gbasedir}/ModFile.txt
ContentFile=${gbasedir}/ContentFile.txt
ServerConfig=${gbasedir}/ServerConfig.txt
fastdl_file=${gbasedir}/FastDLfile.txt




function parse_serverconfig() {
  shopt -s extglob
  tr -d '\r' < $ServerConfig > $ServerConfig.unix
  while IFS='= ' read -r lhs rhs
  do
      if [[ ! $lhs = "~ ^\ *#" && -n $lhs ]]; then
          rhs=${rhs%%\#*}
          rhs=${rhs%%*( )}
          rhs=${rhs%\"*}     
          rhs=${rhs#\"*}     
          export lhs=$rhs
      fi
  done < $ServerConfig.unix
  rm -f $ServerConfig.unix
}
function content_install () {
    gameinfo=$1
    gamename=${gameinfo%-*}
    gameid=${gameinfo#*-}
    echo "installing Content: ${gameid} - ${gamename}"
    echo "folder: ${gbasedir}/game-content"
    /opt/steamcmd/steamcmd.sh +login anonymous +force_install_dir ${gbasedir}/game-content +app_update "${gameid}" validate +quit;
}

main() {
    echo "Running"
    cd ${gbasedir}
    filebrowser -c /home/steam/fm-config.json &

    if [ -f "${fastdl_file}" ]
    then
        fastdl.sh $fastdlfolder
        if [ ! -D "${fastdlfolder}" ]
        then
            echo "FastDL available on port ${fastdl_port}"
            caddy -port ${fastdl_port} -root ${fastdlfolder}
        fi
    fi


    # update Gmod server
    /opt/steamcmd/steamcmd.sh +login anonymous +force_install_dir ${gbasedir} +app_update 4020 validate +quit    
    
    #Process Content Installs
    if [ ! -f "${ContentFile}" ]
    then
    echo "No Content File - Skipping"
    fi
    if [ -f "${ContentFile}" ]
    then
        echo "Found Content File - Installing/updating in ${gbasedir}/game-content"
        while read gamenameid;do content_install "${gamenameid}";done < ${ContentFile}
    fi

    #Process Serverconfig
    if [ ! -f "${ServerConfig}" ]
    then
    echo "No Server Config found - Skipping"
    fi
    if [ -f "${ServerConfig}" ]
    then
        echo "Found Server Config - loading"
        parse_serverconfig
        echo ""
        echo "----------------------------------------------"
        echo " - Server Name: ${GMOD_SERVERNAME}"
        echo ""
        echo " --- Gamemode: ${GMOD_GAMEMODE}"
        echo " --- Map: ${GMOD_MAP}"
        echo " --- Port: ${GMOD_PORT}"
        echo " --- Max Players: ${GMOD_MAXPLAYERS}"
        echo " --- "
        echo ""
        echo "----------------------------------------------"
    fi

    # Process Mod Updates/installs
    if [ ! -f "${ModFile}" ]
    then
        echo "No ModFile - Starting gmod"
        exec tmux new-session -A -s gmodserver ${gbasedir}/srcds_run -steamdir /opt/steamcmd -console -port ${GMOD_PORT} +r_hunkalloclightmaps 0 +sv_hostname ${GMOD_SERVERNAME} +map ${GMOD_MAP} +gamemode ${GMOD_GAMEMODE} +maxplayers "${GMOD_MAXPLAYERS}"
    fi
    if [ -f "${ModFile}" ]
    then
        WORKSHOPID=$(cat $ModFile)
        echo "ModFile found - Starting gmod with mod collection autoupdating"
        echo ""
        echo ""
        echo "Current Workshop ID: ${WORKSHOPID}"
        echo ""
        exec tmux new-session -A -s gmodserver ${gbasedir}/srcds_run -steamdir /opt/steamcmd -console +host_workshop_collection "${WORKSHOPID}" -port ${GMOD_PORT} +r_hunkalloclightmaps 0 +sv_hostname ${GMOD_SERVERNAME} +map ${GMOD_MAP} +gamemode ${GMOD_GAMEMODE} +maxplayers ${GMOD_MAXPLAYERS}
    fi
}
main
